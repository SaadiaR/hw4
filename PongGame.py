# ----------------------------------------------------
# File: PongGame.py
# ----------------------------------------------------
# Author: Sayyeda Saadia Razvi, Sarah Elias, Jake Lewis, BitBucket handle (SaadiaR)
# ----------------------------------------------------
# Plaftorm: Windows 7
# Environment: Python 2.7 
# Libaries: MatPlotLib 1.3.1
#           Tkinter $Revision: 81008 $
#           Math
#           Random
#           Itertools
# ----------------------------------------------------
# Description: 
# This program creates a Pong Game using object oriented classes
# A Ball class and Paddle class create mathematical models of ball and paddles respectively
# A Pong class (using tkinter) creates physical models in GUI 
# Pong class also handles interaction/collision of ball with board and paddles, etc
# ---------------------------------------------------

from random import choice, randint, randrange, randint
import itertools
from math import sin, cos, pi, sqrt
import Tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import sys, winsound
from PIL import ImageTk


def dot(x, y):
    """
    2D dot product
    """
    return x[0]*y[0] + x[1]*y[1]


def scale(x, a):
    """
    2D scalar multiplication
    """
    return (x[0]*a, x[1]*a)


def add(x, y):
    """
    2D vector addition
    """
    return (x[0]+y[0], x[1]+y[1])


class Ball():
    def __init__(self, board_width, board_height, time_interval):
        # create ball with specified radius, color and other initial conditions
        self.radius = 10
        self.color = "red"
        self.initial_conditions(board_width, board_height)
        self.update_position(time_interval)

    def initial_conditions(self, board_width, board_height):
        self.Gx = 0
        self.Gy = 0
        self.G_Angle = 0
        # gives ball velocity with random angle
        # ensures reasonable angle chosen and ball never moves just vertically

        self.velocity = 400
        self.random_velocity_angle(range1=(30,80), range2=(100,150), range3=(210,260), range4=(280,330))

        self.xPos = board_width/2
        self.yPos = board_height/2

    def update_position(self, time_interval): 
        # uses the previous velocity to update the position. Affect of gravity also included.
        self.xPos += (self.xVelocity * time_interval + 0.5*self.Gx*time_interval**2)
        self.yPos += (self.yVelocity * time_interval + 0.5*self.Gy*time_interval**2)
        # updating the velocity. If gravity is zero, the velocity is unchanged
        self.xVelocity += self.Gx * time_interval
        self.yVelocity += self.Gy * time_interval

    def random_velocity_angle(self, range1=(0,360), range2=(0,0), range3=(0,0), range4=(0,0)):
        Angle_range = list(itertools.chain(xrange(range1[0],range1[1]), xrange(range2[0],range2[1]), xrange(range3[0],range3[1]), xrange(range4[0],range4[1])))
        Angle = choice(Angle_range) * pi/180

        self.xVelocity = self.velocity*sin(Angle)
        self.yVelocity = self.velocity*cos(Angle)

    def Gravity_Response(self, state, time_interval):
        if state == 1: # gravity on
            gg = 500
        else: # gravity off
            gg = 0
        G_Angle_range = range(0,360) # degrees
        self.G_Angle = choice(G_Angle_range) * pi/180 # radians

        self.Gx = gg*sin(self.G_Angle )
        self.Gy = gg*cos(self.G_Angle )


    def ball_wall_response(self, board_height):
        #update position when the ball hits the walls so that it begins at wall
        if self.yPos < board_height/2: #collision with top wall (canvas defined from upper left corner)
            self.yPos = self.radius
        elif self.yPos > board_height/2: #collision with bottom wall
            self.yPos = board_height - self.radius
        #update velocity so ball is reflected off wall
        self.yVelocity *= -1

    def ball_miss_response(self, board_width, board_height):
        # reinitialize ball w/ centered position and new velocity
        self.initial_conditions(board_width, board_height)

    def ball_paddle_response(self, board_width, paddle_width):
        # update position of ball so it just touches the paddle 
        if self.xPos < board_width/2: #collision with left paddle
            self.xPos = self.radius + paddle_width
        elif self.xPos > board_width/2: #collision with right paddle
            self.xPos = board_width - self.radius - paddle_width
        # then update velocity
        self.xVelocity *= -1
        #what about other shapes?

    def bounce(self, surface_normal):
        velocity = (self.xVelocity, self.yVelocity)
        diagonal = -2 * dot(surface_normal, velocity)
        velocity = add(velocity, scale(surface_normal, diagonal))
        self.xVelocity = velocity[0]
        self.yVelocity = velocity[1]
 
        #we would like to create a general response function for bouncing. Will do for part 2.
        pass

    def increase_speed(self):
        # increases velocity of ball
            self.xVelocity *= 1.5
            self.yVelocity *= 1.5

    def portal_response(self, portal1_position, portal2_position, board_height):
        if self.yVelocity < 0:
            self.yPos = board_height - self.radius - 1
            self.xPos = portal2_position
        elif self.yVelocity > 0:
            self.yPos = self.radius + 1
            self.xPos = portal1_position

    # def cloud_response(self):
    #     # self.xPos = 

    #     self.random_velocity_angle()


    #     ###### NEED TO UPDATE POSITION SO DOESN'T CONTINUALLY TRIGGER EVENT


    def ball_obstacle_response(self):
        #will probably have to make functions for each type of obstacle
        pass
    
#========================================================================
class Monster(Ball):
    def __init__(self, board_width, board_height, time_interval):
        Ball.__init__(self, board_width, board_height, time_interval)
        self.radius = 50
        self.color = "yellow"
        self.xPos = randrange(board_width/4, board_width*3/4)
        self.yPos = randrange(board_height/4, board_height*3/4)
        self.velocity = 200
        self.random_velocity_angle()
#========================================================================
    
class Paddle():
    def __init__(self, xPos, color, board_height, up, down):
        # Initialize paddle
        self.length = 100
        self.width = 20
        self.color = color
        self.xPos = xPos
        self.initialize_paddle(board_height, up,down)

    def move(self, buttons, board_height):
        # makes up and down buttons for paddles so that pushing and releasing both change velocity
        # this way if you press both buttons you won't move
        in_up_range = (self.yPos - self.length/2 > 0)
        in_down_range = (self.yPos + self.length/2 < board_height)
        # in_range = (self.yPos - self.length/2 > 0) or (self.yPos + self.length/2 < board_height)
        if (self.up_key in buttons and self.down_key not in buttons) and in_up_range:
            self.yPos -= self.velocity
        elif (self.down_key in buttons and self.up_key not in buttons) and in_down_range:
            self.yPos += self.velocity

    def center_paddle(self, board_height):
        #position of mathematical paddle defined from the middle of the outside side
        self.yPos = board_height/2

    def reverse_speed(self):
        self.up_key, self.down_key = self.down_key, self.up_key

    def reset_speed(self, up, down):
        self.up_key = up
        self.down_key = down

    # the above two could be one func only:
    def paddle_key_direction(self, up, down):
        self.up_key, self.down_key = up, down

    def change_shape(self):
        #change shape of paddle
        self.radius = self.length # create semi circle
        pass

    def initialize_paddle(self, board_height, up, down):
        self.velocity = 8
        # reset key control
        self.paddle_key_direction(up, down)
        # self.reset_speed(up, down)
        # self.up_key = up
        # self.down_key = down
        # centralize paddle
        self.center_paddle(board_height)
        # self.yPos = board_height/2



#========================================================================    
class Portal():
    def __init__(self, yPos, board_width):
        self.width = 200
        self.yPos = yPos
        self.color = "chartreuse" #"DeepPink2"
        self.random_position(board_width)

    def random_position(self, board_width):
        self.xPos = randrange(self.width/2, board_width - self.width/2)    
#========================================================================    
    
class Pong(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.master = master
        # ====================================
        self.Initialize_Objects()           # creates ball and paddles from their respective classes
        #=====================================
        #Tkinter initialization
        # controls keyboard
        self.buttons = set()
        self.bind_all('<KeyPress>', lambda event: self.buttons.add(event.keysym))
        self.bind_all('<KeyRelease>', lambda event: self.buttons.discard(event.keysym))

        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        
        self.Create_Widgets()
        self.Draw_Board()
        self.pack()

        # Event detection lists
        self.events = [self.ball_wall_event, 
                       self.ball_paddle_event, 
                       self.ball_miss_event,
                       self.Gravity_ON_Event,
                       self.monster_wall_event,
                       self.monster_side_event,
                       self.monster_move_event,
                       self.ball_monster_event,
                       self.ball_portal_event,
                       self.portal_move_event,
                       lambda: self.paddle_reverse_event(0),
                       lambda: self.paddle_reverse_event(1),
                       self.cloud_event,
                       self.cloud_move_event]
                       # self.Gravity_OFF_Event]

        self.responses = [lambda: self.b1.ball_wall_response(self.board_height),
                          self.ball_paddle_response,
                          self.ball_miss_response,
                          self.Gravity_ON_Response,
                          lambda: self.monster1.ball_wall_response(self.board_height),
                          lambda: self.monster1.ball_paddle_response(self.board_width, self.player1.width),
                          lambda: self.monster1.random_velocity_angle(),
                          self.monster_response,
                          self.ball_portal_response,
                          self.portal_move_response,
                          lambda: self.paddle_reverse_response(0),
                          lambda: self.paddle_reverse_response(1),
                          self.ball_cloud_response,
                          self.cloud_move_response]
                          # self.Gravity_Response(0)]

        # print len(self.events), len(self.responses)
        # print self.events
        # print self.responses

        # Time loop for animation
        self.Update_Ball_Location()
    #==========================================================    

    def Initialize_Objects(self):
        #Variables for game
        self.board_width = 1200
        self.board_height = 700

        self.time_interval = .01
        self.Time = 0
        self.Time_ms = 0
        self.G_time = 10000 #ms
        self.PaddleReverseTime = 9000
        # self.paddle1_speedchange_time = 5000
        # self.paddle2_speedchange_time = 5020

        self.CloudXcorner = self.board_width*0.2 #cloud pos
        self.CloudYcorner = self.board_height*0.2 #cloud pos

        self.player1_score = 0
        self.player2_score = 0

        self.hits = 0 # number of consecutive hits by players
        self.P1hits = 0
        self.P2hits = 0

        self.reset = False # checks if program has been reset

        # ---------------------------------------------------------------------------------
        # Create ball and paddle objects from their classes
        self.b1 = Ball(self.board_width, self.board_height, self.time_interval)
        
        self.player1 = Paddle(0, "blue", self.board_height, 'w', 's')
        self.player2 = Paddle(self.board_width, "blue", self.board_height, 'Up', 'Down')
        
        self.monster1 = Monster(self.board_width, self.board_height, self.time_interval)
        self.portaltop = Portal( 0, self.board_width)
        self.portalbottom = Portal( self.board_height, self.board_width)
 
    #==========================================================

    def Create_Widgets(self):
        # buttons for restart and quit
        self.restart = tk.Button(self, text="Restart", command=self.restart).grid(column=0,row=0, sticky = 'W')
        self.quit = tk.Button(self, text="Quit", command=self.quit).grid(column=1,row=0)

    def Draw_Board(self):
        # create canvas
        self.w = tk.Canvas(self, width=self.board_width, height=self.board_height, bg="black", relief="raised")
        self.w.grid(column=0, row=1, columnspan=2)
        
        self.verticalGradientRectangle(0, 0, self.board_width, self.board_height,   0, 191, 255,    175, 238, 238)
        board_divider = self.w.create_line( self.board_width/2, self.board_height, self.board_width/2, 0, fill="white", dash=(4,4))
        
        #creates physical ball and paddles as well as the score keeper (text)
        self.CreateTKBall()
        self.CreateTKPaddle()
        self.CreateTKMonster()
        self.CreateTKPortal()
        self.CreateTKCloud()
        self.CreateTKArrow()
        self.paddle_rev_statement = self.CreateTKStatements("Paddle movements reversed!", self.board_height*1/4, color="black")         

        self.w.delete(self.G_arrow, self.paddle_rev_statement) #, self.paddle_rev_statement)

        self.Update_score()
        self.Update_clock()

    def verticalGradientRectangle(self, x0, y0, x1, y1,     startRed, startGreen, startBlue,    endRed, endGreen, endBlue):
        y = y0
        while (y <= y1):
            # fade the three color components depending on the y coordinate
            # note that integer division works fine here: why?
            red = (startRed*(y1 - y) + endRed*(y - y0)) / (y1 - y0)
            green = (startGreen*(y1 - y) + endGreen*(y - y0)) / (y1 - y0)
            blue = (startBlue*(y1 - y) + endBlue*(y - y0)) / (y1 - y0)

            # create a hexadecimal color representation
            color = "#%02x%02x%02x" % (red, green, blue)

            # create a colored horizontal line at position y
            self.w.create_line(x0, y, x1, y, fill=color)
            y +=1

    def CreateTKBall(self):
        # physical ball. Positions defined by upper left and lower right corners of a bounding box
        self.ball1 = self.w.create_oval( self.b1.xPos-self.b1.radius, 
                                         self.b1.yPos+self.b1.radius, 
                                         self.b1.xPos+self.b1.radius, 
                                         self.b1.yPos-self.b1.radius, 
                                         fill = self.b1.color)

    def CreateTKPaddle(self):
        # physical paddles. Positions defined by upper left and lower right corners of rectangle. 
        self.paddle1 = self.w.create_rectangle( 0, 
                                              ( self.player1.yPos - self.player1.length/2 ), 
                                                self.player1.width, 
                                              ( self.player1.yPos + self.player1.length/2 ), 
                                                fill = self.player1.color)
        self.paddle2 = self.w.create_rectangle( (self.board_width - self.player2.width), 
                                                (self.player2.yPos - self.player1.length/2), 
                                                 self.board_width, 
                                                (self.player2.yPos + self.player2.length/2), 
                                                 fill = self.player2.color)

    def CreateTKMonster(self, eyecolor ='black', thickness=2):
        # basic guides
        left = self.monster1.xPos - self.monster1.radius
        right = self.monster1.xPos + self.monster1.radius
        width = right - left
        top = self.monster1.yPos - self.monster1.radius
        bottom = self.monster1.yPos + self.monster1.radius
        height = bottom - top
        # quarter guides
        centerleft = (self.monster1.xPos + left) / 2
        centerright = (self.monster1.xPos + right) / 2
        middletop = (self.monster1.yPos + top) / 2
        middlebottom = (self.monster1.yPos + bottom) / 2
        # eye guides
        eyeradius = ((width+height)/2) / 15
        eye1center = (self.monster1.xPos + centerleft) / 2
        eye2center = (self.monster1.xPos + centerright) / 2
        eyemiddle = middletop
        # smile guides 
        smileheight = height / 10
        smiletop = self.monster1.yPos+smileheight
        smileleft = centerleft
        smileright = centerright

        #draw outer circle
        self.w.create_oval(left,top, right,bottom,
                           fill=self.monster1.color, width=thickness, tag = 'Monster')
        #draw eyes
        self.w.create_oval(eye1center-eyeradius,eyemiddle-eyeradius,
                           eye1center+eyeradius,eyemiddle+eyeradius,
                           fill=eyecolor, width=thickness/2, tag = 'Monster')
        self.w.create_oval(eye2center-eyeradius,eyemiddle-eyeradius,
                           eye2center+eyeradius,eyemiddle+eyeradius,
                           fill=eyecolor, width=thickness/2, tag = 'Monster')
        #draw smile
        self.w.create_arc(smileleft,smiletop-smileheight,
                          smileright,smiletop+2*smileheight,
                          start=0, extent=180, width=thickness*2, style=tk.ARC, tag = 'Monster')

    def CreateTKPortal(self):
        self.portal1 = self.w.create_line( (self.portaltop.xPos - self.portaltop.width/2), 0,
                                           (self.portaltop.xPos + self.portaltop.width/2), 0, 
                                           fill = self.portaltop.color, width = 20)
        self.portal2 = self.w.create_line( (self.portalbottom.xPos - self.portalbottom.width/2), self.board_height, 
                                           (self.portalbottom.xPos + self.portalbottom.width/2), self.board_height, 
                                           fill = self.portalbottom.color, width = 10)

    def CreateTKCloud(self):
        self.cloud_width = 250
        self.cloud_height = 150
        color = "white"
        self.w.create_rectangle(self.CloudXcorner, self.CloudYcorner, self.CloudXcorner + self.cloud_width, self.CloudYcorner + self.cloud_height, fill = color, outline = color, tag = "Cloud")

        self.w.create_oval(self.CloudXcorner, self.CloudYcorner-10, self.CloudXcorner + self.cloud_width/4, self.CloudYcorner + 10, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner + self.cloud_width/4, self.CloudYcorner-10, self.CloudXcorner + self.cloud_width/2, self.CloudYcorner + 10, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner + self.cloud_width/2, self.CloudYcorner-10, self.CloudXcorner + 3*self.cloud_width/4, self.CloudYcorner + 10, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner + 3*self.cloud_width/4, self.CloudYcorner-10, self.CloudXcorner + self.cloud_width, self.CloudYcorner + 10, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner-10, self.CloudYcorner, self.CloudXcorner + 10, self.CloudYcorner + self.cloud_height/2, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner-10, self.CloudYcorner+self.cloud_height/2, self.CloudXcorner + 10, self.CloudYcorner + self.cloud_height, fill = color, outline = color, tag = "Cloud")

        self.w.create_oval(self.CloudXcorner, self.CloudYcorner-10+self.cloud_height, self.CloudXcorner + self.cloud_width/4, self.CloudYcorner + 10+self.cloud_height, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner + self.cloud_width/4, self.CloudYcorner-10+self.cloud_height, self.CloudXcorner + self.cloud_width/2, self.CloudYcorner + 10+self.cloud_height, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner + self.cloud_width/2, self.CloudYcorner-10+self.cloud_height, self.CloudXcorner + 3*self.cloud_width/4, self.CloudYcorner + 10+self.cloud_height, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner + 3*self.cloud_width/4, self.CloudYcorner-10+self.cloud_height, self.CloudXcorner + self.cloud_width, self.CloudYcorner + 10+self.cloud_height, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner-10+self.cloud_width, self.CloudYcorner, self.CloudXcorner + 10+self.cloud_width, self.CloudYcorner + self.cloud_height/2, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner-10+self.cloud_width, self.CloudYcorner+self.cloud_height/2, self.CloudXcorner + 10+self.cloud_width, self.CloudYcorner + self.cloud_height, fill = color, outline = color, tag = "Cloud")

        self.photo = tk.PhotoImage(file = 'angry_face.gif')
        self.w.create_image( self.CloudXcorner+self.cloud_width/2, self.CloudYcorner+self.cloud_height/2, image=self.photo, tag = "Cloud")

    def Update_score(self):
        # score displayer
        # p1_score_string = "Player 1: %s" % self.player1_score
        # p2_score_string = "Player 2: %s" % self.player2_score
        # self.p1score = self.w.create_text( self.board_width/2 - 60, 20, text=p1_score_string, fill="white")
        # self.p2score = self.w.create_text( self.board_width/2 + 60, 20, text=p2_score_string, fill="white")
        self.p1score = self.w.create_text( self.board_width/2 - 150, 50, text=self.player1_score, fill="blue", font="Arial 30")
        self.p2score = self.w.create_text( self.board_width/2 + 150, 50, text=self.player2_score, fill="blue", font="Arial 30")

    def Update_clock(self):
        seconds = self.Time%60 - (self.Time%60)%1
        minutes = (self.Time-self.Time%60)/60

        time_string = "%02d:%02d" % (minutes, seconds)
        self.time_display = self.w.create_text(self.board_width/2, 30, text=time_string, fill="white", font="Arial 20") 

    def CreateTKArrow(self):
        self.G_arrow = self.w.create_line( self.board_width/2,
                                            self.board_height/2,
                                            self.board_width/2 + 70*sin(self.b1.G_Angle),
                                            self.board_height/2 + 70*cos(self.b1.G_Angle),
                                            fill = "green", width=2, arrow = "last")

    def CreateTKStatements(self, statement, placement, color):
        return self.w.create_text( self.board_width/2, placement, text=statement, fill=color, font="Arial 18")

    def restart(self):
        # restarts game by setting reset state to True, then reinitializing objects and board
        self.reset = True
        self.Initialize_Objects()
        self.Draw_Board()

    #===========================================================================
    # Paddle Functions
    def update_paddle_math(self):
        # for updating mathematical model
        self.player1.move(self.buttons, self.board_height) #math
        self.player2.move(self.buttons, self.board_height)

    def update_paddle_position(self):
        # for updating tkinter model of paddle1 and paddle2
        self.w.move(self.paddle1, 0, self.player1.yPos - self.w.coords(self.paddle1)[1] - self.player1.length/2) #physical      
        self.w.move(self.paddle2, 0, self.player2.yPos - self.w.coords(self.paddle2)[1] - self.player1.length/2)

    def closest_distance(self):
        #we would like to create a function that detects the closest distance between two objects. will do for part 2
        pass    

    #============================================================================
    # Event functions

    # -----------------------------------------------------------------------------
    # Gravity
    # -----------------------------------------------------------------------------
    def Gravity_ON_Event(self):
        if self.Time_ms >= 10000: # every 10 sec
            return (self.Time_ms) == self.G_time
    def Gravity_ON_Response(self):
        self.G_time = randint(8,12)*1000 + self.Time_ms
        self.w.delete(self.G_arrow)
        self.b1.Gravity_Response(1, self.time_interval)
        # print self.b1.G_Angle, self.b1.Gx, self.b1.Gy
        self.CreateTKArrow()

    # -----------------------------------------------------------------------------
    # Ball with Wall
    # -----------------------------------------------------------------------------
    def ball_wall_event(self):
        #returns TRUE if ball hits either top or bottom wall. otherwise FALSE
        bottom = self.b1.yPos >= self.board_height - self.b1.radius
        top = self.b1.yPos <= self.b1.radius
        portal1_there = (self.portaltop.xPos - self.portaltop.width/2) <= self.b1.xPos <= (self.portaltop.xPos + self.portaltop.width/2)
        portal2_there = (self.portalbottom.xPos - self.portalbottom.width/2) <= self.b1.xPos <= (self.portalbottom.xPos + self.portalbottom.width/2)
        return (top and not portal1_there) or (bottom and not portal2_there)

        # return top or bottom

    # -----------------------------------------------------------------------------
    # Paddle 
    # -----------------------------------------------------------------------------
    def ball_paddle_event(self):
        #returns TRUE if ball hits paddles (meaning ball is in the x and y positions overlapping the paddle). otherwise FALSE
        ball_left_hit = self.b1.xPos <= self.player1.width + self.b1.radius
        ball_right_hit = self.b1.xPos >= self.board_width - self.player2.width - self.b1.radius
        paddle1_there = (self.player1.yPos - self.player1.length/2) <= self.b1.yPos <= (self.player1.yPos + self.player1.length/2)
        paddle2_there = (self.player2.yPos - self.player2.length/2) <= self.b1.yPos <= (self.player2.yPos + self.player2.length/2)
        return (ball_left_hit and paddle1_there) or (ball_right_hit and paddle2_there)
    def ball_paddle_response(self):
        sys.stdout.write("\a") # play beep                
        # calls function responsible for ball's response to hitting paddle
        self.b1.ball_paddle_response(self.board_width, self.player1.width)
        self.w.delete(self.ball1)
        self.CreateTKBall()

        # updates number of consecutive hits to increase game difficulty after every 5 hits
        self.hits += 1
        if self.hits % 5 ==0: # that is, 5, 10, 15...
            self.b1.increase_speed() 
            # self.player1.reverse_speed()
            # self.player2.reverse_speed() 
            # self.paddle_rev_statement = self.CreateTKStatements("Paddles reversed!", self.board_height*1/4, color="white")         

    def ball_miss_event(self):
        #returns TRUE if ball misses the paddle and it "hits" the left or right walls. otherwise FALSE
        ball_left = self.b1.xPos <= 0 - self.b1.radius
        ball_right = self.b1.xPos >= self.board_width + self.b1.radius
        return ball_left or ball_right 
    def ball_miss_response(self): 
        winsound.Beep(400, 95)  # play beep
        #checks to see who gains point 
        if self.b1.xPos < self.board_width/2: # ball on left side of board
            self.player2_score += 1
            self.hits = 0
        if self.b1.xPos > self.board_width/2: # ball on right side of board
            self.player1_score += 1
            self.hits = 0 

        # resets mathematical ball and paddles   
        self.b1.ball_miss_response(self.board_width, self.board_height)
        self.player1.initialize_paddle(self.board_height, 'w','s')
        self.player2.initialize_paddle(self.board_height, 'Up','Down')      

        # resets physical ball and paddles
        self.w.delete(self.ball1, self.paddle1, self.paddle2, self.p1score, self.p2score, self.G_arrow, self.paddle_rev_statement)
        self.CreateTKBall()
        self.CreateTKPaddle()
        # updates score
        self.Update_score()

    def paddle_reverse_event(self, state):
        if state == 1: # On, that is, reverse speed every 9 sec for duration of 3 sec
            if (self.Time_ms % 9000 == 0) and (self.Time_ms >= 9000):
                self.PaddleReverseTime = self.Time_ms
                return True 
        if state == 0: # Off, returns speed to original after 3s of reversal
            return self.Time_ms == self.PaddleReverseTime+3000 and self.Time_ms>0
    def paddle_reverse_response(self, state):      
        if state == 1: # reverse
            self.paddle_rev_statement = self.CreateTKStatements("Paddles reversed!", self.board_height*1/4, color="white")         
            self.player1.paddle_key_direction('s','w')
            self.player2.paddle_key_direction('Down','Up')
            # self.player1.reverse_speed()
            # self.player2.reverse_speed()             
        if state == 0: # normal
            self.w.delete(self.paddle_rev_statement)
            self.player1.paddle_key_direction('w','s')
            self.player2.paddle_key_direction('Up', 'Down')

    # -----------------------------------------------------------------------------
    # portal
    # -----------------------------------------------------------------------------
    def ball_portal_event(self):
        bottom = self.b1.yPos >= self.board_height - self.b1.radius
        top = self.b1.yPos <= self.b1.radius
        portal1_there = (self.portaltop.xPos - self.portaltop.width/2) <= self.b1.xPos <= (self.portaltop.xPos + self.portaltop.width/2)
        portal2_there = (self.portalbottom.xPos - self.portalbottom.width/2) <= self.b1.xPos <= (self.portalbottom.xPos + self.portalbottom.width/2)
        return (top and portal1_there) or (bottom and portal2_there)
    def ball_portal_response(self):
        self.b1.portal_response(self.portaltop.xPos, self.portalbottom.xPos, self.board_height)
        self.w.delete(self.ball1)
        self.CreateTKBall()   
   
        # .................................

    def portal_move_event(self):
        return (self.Time_ms)%10000 ==0
    def portal_move_response(self):
        self.portaltop.random_position(self.board_width)
        self.portalbottom.random_position(self.board_width)
        self.w.delete(self.portal1, self.portal2)
        self.CreateTKPortal()

    # -----------------------------------------------------------------------------
    # monster
    # -----------------------------------------------------------------------------
    def ball_monster_event(self):
        relative_distance = sqrt((self.b1.xPos - self.monster1.xPos)**2 + (self.b1.yPos - self.monster1.yPos)**2)
        monster_ball_distance = self.monster1.radius + self.b1.radius >= relative_distance
        return monster_ball_distance
    def monster_response(self):
        self.w.delete(self.ball1)
        distance_vectorx = self.b1.xPos - self.monster1.xPos
        distance_vectory = self.b1.yPos - self.monster1.yPos
        norm_distance = sqrt((distance_vectorx)**2 + (distance_vectory)**2)
        # norm_distance = self.b1.radius + self.monster1.radius
        normalx = distance_vectorx/norm_distance
        normaly = distance_vectory/norm_distance
        norm_normal = sqrt(normalx**2 + normaly**2)
        # reset ball position
        self.b1.xPos = self.monster1.xPos + normalx * (self.b1.radius + self.monster1.radius)
        self.b1.yPos = self.monster1.yPos + normaly * (self.b1.radius + self.monster1.radius)
        # reset ball velocity
        self.reflect((normalx, normaly))
        # reset physical ball
        self.CreateTKBall()
        # print "Monster", norm_normal
    
    def monster_wall_event(self):
        #returns TRUE if ball hits either top or bottom wall. otherwise FALSE
        bottom = self.monster1.yPos >= self.board_height - self.monster1.radius
        top = self.monster1.yPos <= self.monster1.radius
        return top or bottom 
    def monster_side_event(self):
        monster_left_hit = self.monster1.xPos <= self.player1.width + self.monster1.radius
        monster_right_hit = self.monster1.xPos >= self.board_width - self.player2.width - self.monster1.radius
        return monster_right_hit or monster_left_hit   
    def monster_move_event(self):
        # returns True every 5 sec, after 5 secs into the program
        return (self.Time_ms)%5000 == 0 and self.Time_ms >=5000

    # -----------------------------------------------------------------------------
    # cloud
    # -----------------------------------------------------------------------------
    def cloud_event(self):
        inx = self.CloudXcorner <= self.b1.xPos <= self.CloudXcorner + self.cloud_width
        iny = self.CloudYcorner <= self.b1.yPos <= self.CloudYcorner + self.cloud_height
        return (inx and iny) #and self.Time_ms > 5000
    def ball_cloud_response(self):
        self.w.delete(self.ball1)
        side = randint(1,4)

        if side == 1:
            self.b1.yPos = self.CloudYcorner
            self.b1.xPos = randrange(self.CloudXcorner, self.CloudXcorner + self.cloud_width)
            self.b1.random_velocity_angle(range1=(100,260))
        if side == 3: 
            self.b1.yPos = self.CloudYcorner + self.cloud_height
            self.b1.xPos = randrange(self.CloudXcorner, self.CloudXcorner + self.cloud_width)
            self.b1.random_velocity_angle(range1=(80,280))
        if side == 2:
            self.b1.xPos = self.CloudXcorner
            self.b1.yPos = randrange(self.CloudYcorner, self.CloudYcorner + self.cloud_height)
            self.b1.random_velocity_angle(range1=(190,350))
        if side == 3:
            self.b1.xPos = self.CloudXcorner + self.cloud_width
            self.b1.yPos = randrange(self.CloudYcorner, self.CloudYcorner + self.cloud_height)
            self.b1.random_velocity_angle(range1=(10,1700))
    
        # self.b1.cloud_response()
        self.CreateTKBall()
        #.................................
    def cloud_move_event(self):
        return (self.Time_ms)%9000 == 0 #and self.Time_ms > 0000
    def cloud_move_response(self):
        self.CloudXcorner = randrange(self.board_width*0.1, self.board_width*0.9 - self.cloud_width)
        self.CloudYcorner = randrange(self.board_height*0.1, self.board_height*0.9 - self.cloud_height)
        self.w.delete("Cloud")
        self.CreateTKCloud()
    
    # -----------------------------------------------------------------------------


    #==============================================================================
    def reflect(self, surface):
        """
        Alter the ball's velocity for a perfectly elastic
        collision with a surface defined by the unit normal surface.
        
        surface: a tuple of floats
        """
        velocity = (self.b1.xVelocity,self.b1.yVelocity)
        diagonal = -2 * dot(surface, velocity)
        velocity = add(velocity, scale(surface, diagonal))
        self.b1.xVelocity = velocity[0]
        self.b1.yVelocity = velocity[1]
        # print "Reflect", sqrt(velocity[0]**2 + velocity[1]**2)


    # ============================================================================
    def event_handler(self):
        # checks for an event and calls corresponding event response
        for event, response in zip(self.events, self.responses):
            if event():
                response()

    #=========================================================================================
    # Running a recursive function to update mathematical model, check for events, re-update math, then draw/move in tkinter 
    
    def Update_Ball_Location(self):
        self.w.delete(self.time_display)
        self.Update_clock()

        # checks if program is in a state of reset, if so restarts program
        if self.reset == True:
            self.restart()
            self.reset = False

        # updates mathematical position
        self.b1.update_position(self.time_interval)
        self.monster1.update_position(self.time_interval)
        self.update_paddle_math()

        # runs the event handler to see if mathematical positions require a response
        self.event_handler()

        # updates physical position of the ball and paddle
        dx = self.b1.xVelocity * self.time_interval
        dy = self.b1.yVelocity * self.time_interval

        dxm = self.monster1.xVelocity * self.time_interval
        dym = self.monster1.yVelocity * self.time_interval
        
        self.tt = int(self.time_interval * 1000) # ms (milli seconds)
        self.Time += self.time_interval # current time in s
        self.Time_ms += self.tt # current time in ms
        # print self.Time_ms

        self.update_paddle_position() 
        self.w.move(self.ball1, dx, dy)
        self.w.move('Monster', dxm, dym)

        # time loop to continuously update game
        self.w.after(self.tt, self.Update_Ball_Location)

       
root = tk.Tk()
pong_game = Pong(master=root)
pong_game.master.title("Play Pong!")
pong_game.mainloop()
root.destroy() 
    
