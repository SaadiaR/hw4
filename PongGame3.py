# ----------------------------------------------------
# File: PongGame.py
# ----------------------------------------------------
# Author: Sayyeda Saadia Razvi, Sarah Elias, Jake Lewis, BitBucket handle (SaadiaR)
# ----------------------------------------------------
# Plaftorm: Windows 7
# Environment: Python 2.7 
# Libaries: MatPlotLib 1.3.1
#           Tkinter $Revision: 81008 $
#           Math
#           Random
#    
# ----------------------------------------------------
# Description: 
# This program creates a Pong Game using object oriented classes
# A Ball class and Paddle class create mathematical models of ball and paddles respectively
# A Pong class (using tkinter) creates physical models in GUI 
# Pong class also handles interaction/collision of ball with board and paddles, etc
# ---------------------------------------------------

from random import choice, randint
from math import sin, cos, pi, sqrt
import Tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import sys 
# import winsound
from PongObjects import Ball, Monster, Paddle, Portal
    
#========================================================================
class Pong(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.master = master
        # ====================================
        self.Initialize_Objects() # creates ball and paddles from their respective classes
        #=====================================
        # Tkinter initialization
        # controls keyboard
        self.buttons = set()
        self.bind_all('<KeyPress>', lambda event: self.buttons.add(event.keysym))
        self.bind_all('<KeyRelease>', lambda event: self.buttons.discard(event.keysym))

        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        
        self.Create_Widgets()
        self.Initialize_Bird()
        self.Draw_Board()
        self.pack()

        # dictionary of events and responses
        self.event_response = {# ball hit top/bottom wall
                            lambda: self.event_block_miss(self.b1.radius, self.b1.yPos, self.b1.xPos, self.board_height, self.portalbottom.xPos, self.portaltop.xPos, self.portalbottom.width, self.portaltop.width, 0, 0) : lambda: self.b1.bounce_response(self.b1.yPos, (0,1), (0,-1), self.board_height, 0),
                            # ball hit paddle
                            lambda: self.event_block_hit(self.b1.radius, self.b1.xPos, self.b1.yPos, self.board_width, self.player2.yPos, self.player1.yPos, self.player2.length, self.player1.length, self.player2.width, self.player1.width) : self.ball_paddle_response,
                            # paddle miss ball
                            lambda: self.event_block_miss(self.b1.radius, self.b1.xPos, self.b1.yPos, self.board_width, self.player2.yPos, self.player1.yPos, self.player2.length, self.player1.length, 0, 0) : self.ball_miss_response,
                            # monster hit top/bottom wall
                            lambda: self.monster_wall_event(self.monster1.yPos, self.board_height, 0) : lambda: self.monster1.bounce_response(self.monster1.yPos, (0,1), (0,-1), self.board_height, 0),
                            # monster hit side wall
                            lambda: self.monster_wall_event(self.monster1.xPos, self.board_width, self.player1.width) : lambda: self.monster1.bounce_response(self.monster1.xPos, (1,0), (-1,0), self.board_width, self.player1.width),
                            # move monster randomly
                            lambda: self.obstacle_move_event(5) : self.monster1.random_velocity_angle,
                            # ball hit monster
                            lambda: self.spherical_collision_detect(self.monster1) : lambda: self.monster_response(self.monster1),
                            # ball go through portal
                            lambda: self.event_block_hit(self.b1.radius, self.b1.yPos, self.b1.xPos, self.board_height, self.portalbottom.xPos, self.portaltop.xPos, self.portalbottom.width, self.portaltop.width, 0, 0) : self.ball_portal_response,
                            # portal move randomly
                            lambda: self.obstacle_move_event(10) : self.portal_move_response,
                            # gravity field on
                            self.Gravity_ON_Event : self.Gravity_ON_Response,
                            # reverses the direction of the paddle keys
                            lambda: self.Event_timings(eventname = "paddle normal", state=0, frequency = 9000, duration=3000) : lambda: self.paddle_reverse_response(0),
                            lambda: self.Event_timings(eventname = "paddle reverse", state=1, frequency = 9000, duration=3000) : lambda: self.paddle_reverse_response(1),
                            # ball hit cloud
                            self.cloud_event : self.ball_cloud_response,
                            # cloud move
                            lambda: self.obstacle_move_event(10) : self.cloud_move_response,
                            # change paddle shape
                            lambda: self.Event_timings(eventname = "paddle shape changed", state=1, frequency = 7000, duration=3000) : lambda: self.paddle_shape_response(1),
                            lambda: self.Event_timings(eventname = "paddle shape normal", state=0, frequency = 7000, duration=3000) : lambda: self.paddle_shape_response(0),
                            # ball hitting paddle with semi-circle shape
                            lambda: self.round_paddle_detect(self.player1) : lambda: self.spherical_response(self.player1),
                            lambda: self.round_paddle_detect(self.player2) : lambda: self.spherical_response(self.player2),
                            # ball hitting bird
                            lambda: self.spherical_collision_detect(self.bird1) : lambda: self.bird_response(self.bird1, 'bird1'),
                            lambda: self.spherical_collision_detect(self.bird2) : lambda: self.bird_response(self.bird2, 'bird2'),
                            lambda: self.spherical_collision_detect(self.bird3) : lambda: self.bird_response(self.bird3, 'bird3')}


        # Time loop for animation
        self.Update_Ball_Location()
    #==========================================================    

    def Initialize_Objects(self):
        self.random_factor = [.2,.4,.6,.8]

        self.scene = "day"

        #Variables for game
        self.board_width = 1000
        self.board_height = 650

        self.time_interval = .01 # time per loop iteration
        self.Time = 0
        self.Time_ms = 0
        self.G_time = 10000 #ms
        self.EventStartTime = 0 # time that a particular event happened
        self.reversepaddle = 0 # 0 if normal, 1 if reverses

        self.CloudXcorner = choice([0.2,0.4,0.6])*self.board_width
        self.CloudYcorner = choice([0.2,0.4,0.6])*self.board_height

        self.player1_score = 0
        self.player2_score = 0

        self.hits = 0 # number of consecutive hits by players   
        self.reset = False # checks if program has been reset

        #==========================================================
        # Create ball, paddle, monster, portal objects from their classes
        self.b1 = Ball(self.board_width, self.board_height, self.time_interval)
        
        self.player1 = Paddle(0, "blue", self.board_height, 'w', 's')
        self.player2 = Paddle(self.board_width, "blue", self.board_height, 'Up', 'Down')
        
        self.monster1 = Monster(70, "yellow", self.board_width, self.board_height, self.time_interval)
        
        self.portaltop = Portal( 0, self.board_width)
        self.portalbottom = Portal( self.board_height, self.board_width)

        self.Initialize_Bird()
 
    def Draw_Board(self):
        #create canvas
        self.w = tk.Canvas(self, width=self.board_width, height=self.board_height, bg="black", relief="raised")
        self.w.grid(column=0, row=1, columnspan=2)

        # creates physical ball, paddles, portal, moster(sun), birds, gravity arrow, as well as the score keeper (text)
        self.CreateTKBall()
        self.CreateTKPaddle()
        self.CreateTKMonster()
        self.CreateTKPortal()
        self.CreateTKCloud()
        self.CreateTKArrow()
        self.CreateTKBirds()
        self.paddle_rev_statement = self.CreateTKStatements("Paddle reversed!", self.board_height*1/4, color="black")         
  
        # creates a background 
        self.CreateBackground(self.scene) 

        # delete arrow and paddle reversal statement initially, will be re-created when event called
        self.w.delete(self.G_arrow,self.paddle_rev_statement)

        # update clock and score
        self.Update_score()
        self.Update_clock()

    def Create_Widgets(self):
        # buttons for restart and quit
        self.restart = tk.Button(self, text="Restart", command=self.restart).grid(column=0,row=0, sticky = 'W')
        self.quit = tk.Button(self, text="Quit", command=self.quit).grid(column=1,row=0)

    #==========================================================

    def radialGradientCircle(self, x0, y0, x1, y1,  startRed, startGreen, startBlue,      endRed, endGreen, endBlue,  name):
        # we need the x, y ratio to compute y given x
        xyRatio = float(x1 - x0) / float(y1 - y0)
        width = x1 - x0
        height = y1 - y0
        xMiddle = (x0 + x1) / 2
        x = x0
        y = y0

        while (x <= xMiddle): # we need to loop only half way through the rectangle
            # fade the three color components depending on the distance
            # from the center
            red = (startRed*(xMiddle - x) + endRed*(x - x0)) / (xMiddle - x0)
            green = (startGreen*(xMiddle - x) + endGreen*(x - x0)) / (xMiddle - x0)
            blue = (startBlue*(xMiddle - x) + endBlue*(x - x0)) / (xMiddle - x0)

            # create a hexadecimal color representation
            color = "#%02x%02x%02x" % (red, green, blue)
            
            # create a colored rectangle
            self.w.create_oval(x, y, x+width, y+height, fill=color, outline=color, dash=(10,4), stipple="gray12",  tag=name, )
            x += 1
            y += 1.0 / xyRatio
            width -= 2
            height -= 2.0 / xyRatio

    def CreateBackground(self, scene):
        self.board_divider = self.w.create_line( self.board_width/2, self.board_height, self.board_width/2, 0, fill="white", dash=(4,4))
        self.w.lower(self.board_divider)
        # creates a background 
        if scene == "day":
            self.sky = tk.PhotoImage(file= "daysky4.gif") 
            self.background = self.w.create_image(self.board_width/2,self.board_height/2, image=self.sky)
        elif scene == "night":
            self.sky = tk.PhotoImage(file="nightsky3.gif")
            self.background = self.w.create_image(self.board_width/2,self.board_height/2, image=self.sky)
        self.w.lower(self.board_divider)
        self.w.lower(self.background)


    def CreateTKBall(self):
        # physical ball. Positions defined by upper left and lower right corners of a bounding box
        self.ball1 = self.w.create_oval( self.b1.xPos-self.b1.radius, 
                                         self.b1.yPos+self.b1.radius, 
                                         self.b1.xPos+self.b1.radius, 
                                         self.b1.yPos-self.b1.radius, 
                                         fill = self.b1.color)

    def CreateTKPaddle(self):
        # physical paddles. Positions defined by upper left and lower right corners of rectangle. 
        self.paddle1 = self.w.create_rectangle( 0, 
                                              ( self.player1.yPos - self.player1.length/2 ), 
                                                self.player1.width, 
                                              ( self.player1.yPos + self.player1.length/2 ), 
                                                fill = self.player1.color)
        self.paddle2 = self.w.create_rectangle( (self.board_width - self.player2.width), 
                                                (self.player2.yPos - self.player1.length/2), 
                                                 self.board_width, 
                                                (self.player2.yPos + self.player2.length/2), 
                                                 fill = self.player2.color)

    def CreateTKMonster(self, scene="day"):
        # basic guides
        left = self.monster1.xPos - self.monster1.radius
        right = self.monster1.xPos + self.monster1.radius
        width = right - left
        top = self.monster1.yPos - self.monster1.radius
        bottom = self.monster1.yPos + self.monster1.radius
        height = bottom - top 

        if scene=="day":
            self.w.create_oval(left,top, right,bottom, fill="gold", tag = 'Monster')

            eyecolor ='black'
            thickness=2
            # quarter guides
            centerleft = (self.monster1.xPos + left) / 2
            centerright = (self.monster1.xPos + right) / 2
            middletop = (self.monster1.yPos + top) / 2
            middlebottom = (self.monster1.yPos + bottom) / 2
            # eye guides
            eyeradius = ((width+height)/2) / 15
            eye1center = (self.monster1.xPos + centerleft) / 2
            eye2center = (self.monster1.xPos + centerright) / 2
            eyemiddle = middletop+6
            # smile guides 
            smileheight = height / 8
            smiletop = self.monster1.yPos+5
            smileleft = centerleft
            smileright = centerright

            #draw rays
            self.w.create_line(right, self.monster1.yPos, right + 20, self.monster1.yPos,width=thickness,fill=self.monster1.color, tag = 'Monster')
            self.w.create_line(left - 20, self.monster1.yPos, left, self.monster1.yPos,width=thickness,fill=self.monster1.color, tag = 'Monster')
            self.w.create_line(self.monster1.xPos, top, self.monster1.xPos, top - 20,width=thickness,fill=self.monster1.color, tag = 'Monster')
            self.w.create_line(self.monster1.xPos, bottom, self.monster1.xPos, bottom + 20,width=thickness,fill=self.monster1.color, tag = 'Monster')
            self.w.create_line(self.monster1.xPos + self.monster1.radius/sqrt(2), self.monster1.yPos + self.monster1.radius/sqrt(2), self.monster1.xPos + self.monster1.radius/sqrt(2) + 20, self.monster1.yPos + self.monster1.radius/sqrt(2) + 20,width=thickness,fill=self.monster1.color, tag = 'Monster')
            self.w.create_line(self.monster1.xPos + self.monster1.radius/sqrt(2), self.monster1.yPos - self.monster1.radius/sqrt(2), self.monster1.xPos + self.monster1.radius/sqrt(2) + 20, self.monster1.yPos - self.monster1.radius/sqrt(2) - 20,width=thickness,fill=self.monster1.color, tag = 'Monster')
            self.w.create_line(self.monster1.xPos - self.monster1.radius/sqrt(2), self.monster1.yPos + self.monster1.radius/sqrt(2), self.monster1.xPos - self.monster1.radius/sqrt(2) - 20, self.monster1.yPos + self.monster1.radius/sqrt(2) + 20,width=thickness,fill=self.monster1.color, tag = 'Monster')
            self.w.create_line(self.monster1.xPos - self.monster1.radius/sqrt(2), self.monster1.yPos - self.monster1.radius/sqrt(2), self.monster1.xPos - self.monster1.radius/sqrt(2) - 20, self.monster1.yPos - self.monster1.radius/sqrt(2) - 20,width=thickness,fill=self.monster1.color, tag = 'Monster')

            # draw outer circle
            self.w.create_oval(left,top, right,bottom,
                               fill=self.monster1.color, width=thickness, tag = 'Monster')
            # draw eyes
            self.w.create_oval(eye1center-eyeradius,eyemiddle-eyeradius,
                               eye1center+eyeradius,eyemiddle+eyeradius,
                               fill=eyecolor, width=thickness/2, tag = 'Monster')
            self.w.create_oval(eye2center-eyeradius,eyemiddle-eyeradius,
                               eye2center+eyeradius,eyemiddle+eyeradius,
                               fill=eyecolor, width=thickness/2, tag = 'Monster')
            # draw smile
            self.w.create_arc(smileleft,smiletop-smileheight,
                              smileright,smiletop+2*smileheight,
                              start=0, extent=-180, width=thickness*2, style=tk.ARC, tag = 'Monster')
        
        if scene=="night":
            halo = self.radialGradientCircle(left-15,top-15, right+15, bottom+15, 72,61,139,  255,255,240   , 'Monster') #    238,238,224)
            self.w.create_oval(left,top, right,bottom, fill="ivory", tag = 'Monster')
            self.moon1 = tk.PhotoImage(file="moon1.gif").subsample(2,2)
            self.w.create_image(self.monster1.xPos-1, self.monster1.yPos-1, image=self.moon1, tag='Monster')
        
        self.w.lower("Monster")

    def CreateTKPortal(self):
        # create TKinter portals as thick lines
        self.portal1 = self.w.create_line( (self.portaltop.xPos - self.portaltop.width/2), 0,
                                           (self.portaltop.xPos + self.portaltop.width/2), 0, 
                                           fill = self.portaltop.color, width = 20)
        self.portal2 = self.w.create_line( (self.portalbottom.xPos - self.portalbottom.width/2), self.board_height, 
                                           (self.portalbottom.xPos + self.portalbottom.width/2), self.board_height, 
                                           fill = self.portalbottom.color, width = 10)

    def CreateTKCloud(self):
        # dimensions, will be used for detecting collisions
        self.cloud_width = 200
        self.cloud_height = 100

        color = "Lavender" # "alice blue"
        self.w.create_rectangle(self.CloudXcorner, self.CloudYcorner, self.CloudXcorner + self.cloud_width, self.CloudYcorner + self.cloud_height, fill = color, outline = color, tag = "Cloud")

        self.w.create_oval(self.CloudXcorner, self.CloudYcorner-10, self.CloudXcorner + self.cloud_width/4, self.CloudYcorner + 10, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner + self.cloud_width/4, self.CloudYcorner-10, self.CloudXcorner + self.cloud_width/2, self.CloudYcorner + 10, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner + self.cloud_width/2, self.CloudYcorner-10, self.CloudXcorner + 3*self.cloud_width/4, self.CloudYcorner + 10, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner + 3*self.cloud_width/4, self.CloudYcorner-10, self.CloudXcorner + self.cloud_width, self.CloudYcorner + 10, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner-10, self.CloudYcorner, self.CloudXcorner + 10, self.CloudYcorner + self.cloud_height/2, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner-10, self.CloudYcorner+self.cloud_height/2, self.CloudXcorner + 10, self.CloudYcorner + self.cloud_height, fill = color, outline = color, tag = "Cloud")

        self.w.create_oval(self.CloudXcorner, self.CloudYcorner-10+self.cloud_height, self.CloudXcorner + self.cloud_width/4, self.CloudYcorner + 10+self.cloud_height, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner + self.cloud_width/4, self.CloudYcorner-10+self.cloud_height, self.CloudXcorner + self.cloud_width/2, self.CloudYcorner + 10+self.cloud_height, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner + self.cloud_width/2, self.CloudYcorner-10+self.cloud_height, self.CloudXcorner + 3*self.cloud_width/4, self.CloudYcorner + 10+self.cloud_height, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner + 3*self.cloud_width/4, self.CloudYcorner-10+self.cloud_height, self.CloudXcorner + self.cloud_width, self.CloudYcorner + 10+self.cloud_height, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner-10+self.cloud_width, self.CloudYcorner, self.CloudXcorner + 10+self.cloud_width, self.CloudYcorner + self.cloud_height/2, fill = color, outline = color, tag = "Cloud")
        self.w.create_oval(self.CloudXcorner-10+self.cloud_width, self.CloudYcorner+self.cloud_height/2, self.CloudXcorner + 10+self.cloud_width, self.CloudYcorner + self.cloud_height, fill = color, outline = color, tag = "Cloud")

        self.photo = tk.PhotoImage(file = 'angry_face2.gif')
        self.w.create_image( self.CloudXcorner+self.cloud_width/2, self.CloudYcorner+self.cloud_height/2, image=self.photo, tag = "Cloud")

    # create mathematical bird
    def Initialize_Bird(self):        # bird are objects from monster class, only they have 0 velocity
        self.bird1 = Monster(44, 'firebrick2', self.board_width, self.board_height, self.time_interval)
        self.bird2 = Monster(44, 'black', self.board_width, self.board_height, self.time_interval)
        self.bird3 = Monster(44, 'gold', self.board_width, self.board_height, self.time_interval)
    
    # create multiple birds
    def CreateTKBirds(self):
        self.birdpic1 = tk.PhotoImage(file = 'angry_bird1.gif').subsample(2,2)
        self.birdpic2 = tk.PhotoImage(file = 'angry_bird2.gif').subsample(2,2)
        self.birdpic3 = tk.PhotoImage(file = 'angry_bird3.gif').subsample(2,2)

        self.CreateTKBird(self.bird1, "bird1", self.birdpic1)
        self.CreateTKBird(self.bird2, "bird2", self.birdpic2)
        self.CreateTKBird(self.bird3, "bird3", self.birdpic3)

    # creat one TKitner pyhsical bird
    def CreateTKBird(self, bird, name, pic):
        self.w.create_oval( bird.xPos-bird.radius, 
                            bird.yPos+bird.radius, 
                            bird.xPos+bird.radius, 
                            bird.yPos-bird.radius, 
                            fill = bird.color, outline = bird.color ,
                            tag = name)
        pic = self.w.create_image(bird.xPos, bird.yPos, image = pic, tag = name)
        self.w.scale( pic, bird.xPos, bird.yPos, 2, 2)

    def Update_score(self):
        # score displayer
        self.p1score = self.w.create_text( self.board_width/2 - 150, 50, text=self.player1_score, fill="blue", font="Arial 30")
        self.p2score = self.w.create_text( self.board_width/2 + 150, 50, text=self.player2_score, fill="blue", font="Arial 30")

    def Update_clock(self):
        # displays time
        seconds = self.Time%60 - (self.Time%60)%1
        minutes = (self.Time-self.Time%60)/60

        time_string = "%02d:%02d" % (minutes, seconds)
        self.time_display = self.w.create_text(self.board_width/2, 30, text=time_string, fill="white", font="Arial 20") 

    def CreateTKArrow(self):
        # display an arrow to show direction of gravity field
        self.G_arrow = self.w.create_line( self.board_width/2,
                                            self.board_height/2,
                                            self.board_width/2 + 70*sin(self.b1.G_Angle),
                                            self.board_height/2 + 70*cos(self.b1.G_Angle),
                                            fill = "red", width=2, arrow = "last")

    def CreateTKStatements(self, statement, placement, color):
        return self.w.create_text( self.board_width/2, placement, text=statement, fill=color, font="Arial 14")

    def restart(self):
        # restarts game by setting reset state to True, then reinitializing objects and board
        self.reset = True
        self.Initialize_Objects()
        self.Draw_Board()

    #===========================================================================
    # Paddle Functions
    def update_paddle_math(self):
        # for updating mathematical model
        self.player1.move(self.buttons, self.board_height) #math
        self.player2.move(self.buttons, self.board_height)

    def update_paddle_position(self):
        # for updating tkinter model of paddle1 and paddle2
        self.w.move(self.paddle1, 0, self.player1.yPos - self.w.coords(self.paddle1)[1] - self.player1.length/2) #physical      
        self.w.move(self.paddle2, 0, self.player2.yPos - self.w.coords(self.paddle2)[1] - self.player1.length/2)

    #==============================================================
    # WALLS WITH PADDLES/PORTALS
    #==============================================================
    
    # The next two functions are the event detectors for all the walls. Since the side walls with the paddles
    # are similar to the top/bottom walls with portals, we have one detector for paddle/portal and one for hitting
    # the walls (although there aren't technically side walls).
    # ball position is x or y depending on which walls you're dealing with
    # block positions, lengths, and widths are for the portals/paddles

    def event_block_hit(self, radius, ball_position1, ball_position2, boundary, block_position1, block_position2, block_length1, block_length2, block_width1, block_width2):
      # This should apply for hitting paddle or portal: 
        check1 = ball_position1 + radius >= boundary - block_width1
        check2 = ball_position1 - radius <= block_width2
        block1 = (block_position1 - block_length1/2) <= ball_position2 <= (block_position1 + block_length1/2)
        block2 = (block_position2 - block_length2/2) <= ball_position2 <= (block_position2 + block_length2/2)
        return (check1 and block1) or (check2 and block2)

    def event_block_miss(self, radius, ball_position1, ball_position2, boundary, block_position1, block_position2, block_length1, block_length2, block_width1, block_width2):
      # This should apply for missing ball or hitting top/bottom walls
        check1 = ball_position1 + radius >= boundary - block_width1
        check2 = ball_position1 - radius <= block_width2
        block1 = (block_position1 - block_length1/2) <= ball_position2 <= (block_position1 + block_length1/2)
        block2 = (block_position2 - block_length2/2) <= ball_position2 <= (block_position2 + block_length2/2)
        return (check1 and not block1) or (check2 and not block2)

    def ball_miss_response(self): 
        # winsound.Beep(400, 95)  # play beep
        #checks to see who gains point 
        if self.b1.xPos < self.board_width/2: # ball on left side of board
            self.player2_score += 1
            self.hits = 0
        if self.b1.xPos > self.board_width/2: # ball on right side of board
            self.player1_score += 1
            self.hits = 0 

        # resets mathematical ball and paddles   
        self.b1.ball_miss_response(self.board_width, self.board_height)
        self.player1.initialize_paddle(self.board_height, 'w','s')
        self.player2.initialize_paddle(self.board_height, 'Up','Down')      

        # resets physical ball and paddles
        self.w.delete(self.ball1, self.paddle1, self.paddle2, self.p1score, self.p2score, self.G_arrow, self.paddle_rev_statement, 'bird1', 'bird2', 'bird3')
        self.CreateTKBall()
        self.CreateTKPaddle()
        self.reversepaddle = 0

        self.Initialize_Bird()
        self.CreateTKBirds()
        # updates score
        self.Update_score()

    def ball_paddle_response(self):
        # calls function responsible for ball's response to hitting paddle
        self.b1.bounce_response(self.b1.xPos, (1,0), (-1,0), self.board_width, self.player1.width)
        self.w.delete(self.ball1)
        self.CreateTKBall()

        # updates number of consecutive hits to increase game difficulty after every 5 hits
        self.hits += 1
        if self.hits % 5 ==0: # that is, 5, 10, 15...
            self.b1.increase_speed() 
           
    def portal_move_response(self):
        # move the portal to a new random position
        self.portaltop.random_position(self.board_width)
        self.portalbottom.random_position(self.board_width)
        # delete the old physical portal and redraw a new one
        self.w.delete(self.portal1, self.portal2)
        self.CreateTKPortal()  

    def ball_portal_response(self):
      # reset math position of ball, delete physical ball and recreate new one 
        self.b1.portal_response(self.portaltop.xPos, self.portalbottom.xPos, self.board_height)
        self.w.delete(self.ball1)
        self.CreateTKBall() 
    #==============================================================
    # MONSTER/BIRD
    #==============================================================
    def spherical_collision_detect(self, sphere):
      # generic function to detect collision for monster and ball, and ball with semi-circular paddle
        relative_distance = sqrt((self.b1.xPos - sphere.xPos)**2 + (self.b1.yPos - sphere.yPos)**2)
        objects_distance = (sphere.radius + self.b1.radius) >= relative_distance
        return objects_distance

    def monster_wall_event(self, position, board, paddle):
      # checks if monster collides with wall
        checka = position >= board - paddle - self.monster1.radius
        checkb = position <= paddle + self.monster1.radius
        return checka or checkb

    def spherical_response(self, object1):
        # generic response for ball after collision with any spherical body
        # find distance b/w ball and object
        distance_vectorx = self.b1.xPos - object1.xPos
        distance_vectory = self.b1.yPos - object1.yPos
        norm_distance = sqrt((distance_vectorx)**2 + (distance_vectory)**2)
        # norm_distance = self.b1.radius + self.monster1.radius
        normalx = distance_vectorx/norm_distance
        normaly = distance_vectory/norm_distance
        norm_normal = sqrt(normalx**2 + normaly**2)
        # reset ball position
        self.b1.xPos = object1.xPos + normalx * (self.b1.radius + object1.radius)
        self.b1.yPos = object1.yPos + normaly * (self.b1.radius + object1.radius)
        # reset ball velocity
        self.b1.reflect((normalx, normaly))
        # reset physical ball
        self.w.delete(self.ball1)
        self.CreateTKBall()

    def monster_response(self, monster):
        # call generic spherical response
        self.spherical_response(monster)

        # check exception: if ball after bouncing touches monster again
        next_relative_distance = sqrt((self.b1.xPos + self.b1.xVelocity*self.time_interval - self.monster1.xPos - self.monster1.xVelocity*self.time_interval)**2 + (self.b1.yPos + self.b1.yVelocity*self.time_interval- self.monster1.yPos - self.monster1.yVelocity*self.time_interval)**2)
        monster_ball_collide_next =  next_relative_distance <= (self.monster1.radius + self.b1.radius) 
        
        # move ball away again
        if monster_ball_collide_next:
            self.b1.xPos += self.monster1.xVelocity*self.time_interval
            self.b1.yPos += self.monster1.yVelocity*self.time_interval

        # delete old monster and create new themed background and monster
        self.w.delete("Monster")
        
        if self.scene == "day":
            self.scene = "night"
        elif self.scene == "night":
            self.scene = "day"      

        self.CreateTKMonster(scene=self.scene)
        self.CreateBackground(self.scene)

        # speed up ball
        self.b1.xVelocity *= 1.05
        self.b1.yVelocity *= 1.05

    def bird_response(self, bird, TKbird):
        self.spherical_response(bird)
        # also delete TK bird and put mathematical bird off screen
        self.w.delete(TKbird)
        bird.xPos, bird.yPos = -25, -25

    #==============================================================
    # GRAVITY
    #==============================================================
    def Gravity_ON_Response(self):
        # creates a new random time for gravity to start next time
        self.G_time = randint(8,12)*1000 + self.Time_ms
        self.b1.Gravity_Response(1, self.time_interval) 
        self.w.delete(self.G_arrow) # delete old arrow
        self.CreateTKArrow() # make new arrow
  
    #==============================================================
    # CLOUD
    #==============================================================
    def cloud_event(self):   # check if all is inside cloud
        inx = self.CloudXcorner <= self.b1.xPos <= self.CloudXcorner + self.cloud_width
        iny = self.CloudYcorner <= self.b1.yPos <= self.CloudYcorner + self.cloud_height
        return (inx and iny)

    def cloud_move_response(self): # reset cloud to new random position
        self.CloudXcorner = choice([0.2,0.4,.6])*self.board_width
        self.CloudYcorner = choice([0.2,0.4,.6])*self.board_height
        self.w.delete("Cloud") # delete old cloud
        self.CreateTKCloud() # create new one

    def ball_cloud_response(self): # move ball to random edge of cloud and give new random velocity
        side = randint(1,4) # sides 1,2,3,4

        if side == 1:
            self.b1.yPos = self.CloudYcorner
            self.b1.xPos = self.CloudXcorner + self.cloud_width/2 
            self.b1.random_velocity_angle([135,225]) 
        if side == 3: 
            self.b1.yPos = self.CloudYcorner + self.cloud_height
            self.b1.xPos = self.CloudXcorner + self.cloud_width/2 
            self.b1.random_velocity_angle([315,45]) 
        if side == 2:
            self.b1.xPos = self.CloudXcorner
            self.b1.yPos = self.CloudYcorner + self.cloud_height/2 
            self.b1.random_velocity_angle([225,315])
        if side == 3:
            self.b1.xPos = self.CloudXcorner + self.cloud_width
            self.b1.yPos = self.CloudYcorner + self.cloud_height/2 
            self.b1.random_velocity_angle([45,135])
    
        self.w.delete(self.ball1)        
        self.CreateTKBall()

    #==============================================================
    # PADDLE CHANGES
    #==============================================================
    def paddle_shape_response(self, state):
        if state == 1: # response for shape change
            self.reversepaddle = 1 # set reversepaddle to true (=1)
            self.w.delete(self.paddle1, self.paddle2) # delete old paddles and create new shape

            self.paddle1 = self.w.create_oval( -self.player1.radius, 
                                            ( self.player1.yPos - self.player1.radius ), 
                                              self.player1.radius, 
                                            ( self.player1.yPos + self.player1.radius), 
                                              fill = self.player1.color)
            self.paddle2 = self.w.create_oval( (self.board_width - self.player2.radius), 
                                               (self.player2.yPos - self.player1.radius), 
                                                self.board_width + self.player2.radius, 
                                               (self.player2.yPos + self.player2.radius), 
                                                fill = self.player2.color)
                                                     
        if state == 0: # response for revert to original shape
            self.reversepaddle = 0 # set reversepaddle to false (=0)
            self.w.delete(self.paddle1, self.paddle2)
            self.CreateTKPaddle() 

    def round_paddle_detect(self, object1):
        # True only if paddle is reversed AND there is collision with spherical paddle
        return (self.reversepaddle == 1) and self.spherical_collision_detect(object1)

    def paddle_reverse_response(self, state):      
        if state == 1: # reverse
            self.paddle_rev_statement = self.CreateTKStatements("Paddles reversed!", self.board_height*0.9, color="Slate Blue")         
            self.player1.paddle_key_direction(up = 's', down = 'w')
            self.player2.paddle_key_direction(up = 'Down', down = 'Up')            
        if state == 0: # normal
            self.w.delete(self.paddle_rev_statement)
            self.player1.paddle_key_direction(up = 'w', down = 's')
            self.player2.paddle_key_direction(up = 'Up', down = 'Down')

    #==============================================================
    #TIMING
    #==============================================================
    def Gravity_ON_Event(self):
        if self.Time_ms >= 10000: # start checking after 10 secs
            return (self.Time_ms) == self.G_time

    def obstacle_move_event(self, sec):
        return (self.Time_ms)%(1000 * sec) == 0 and self.Time_ms > 0

    def Event_timings(self, eventname, state, frequency, duration):
        if state == 1: # On, that is, reverse speed every 9 sec for duration of 3 sec 
            if (self.Time_ms % frequency == 0) and (self.Time_ms > 0):
                self.EventStartTime = self.Time_ms
                return True 
        if state == 0: # Off, returns speed to original after 3s of reversal
            return (self.Time_ms == self.EventStartTime+duration) and self.Time_ms>0

    #==============================================================
    def event_handler(self):
        for event, response in self.event_response.items():
            if event(): response()
    #=========================================================================================
    
    # Running a recursive function to update mathematical model, check for events, re-update math, then draw/move in tkinter 
    def Update_Ball_Location(self):
        self.w.delete(self.time_display)
        self.Update_clock()

        # checks if program is in a state of reset, if so restarts program
        if self.reset == True:
            self.restart()
            self.reset = False

        # updates mathematical position
        self.b1.update_position(self.time_interval)
        self.monster1.update_position(self.time_interval)
        self.update_paddle_math()

        # runs the event handler to see if mathematical positions require a response
        self.event_handler()

        # updates physical position of the ball and paddle
        dx = self.b1.xVelocity * self.time_interval
        dy = self.b1.yVelocity * self.time_interval

        dxm = self.monster1.xVelocity * self.time_interval
        dym = self.monster1.yVelocity * self.time_interval
        
        self.tt = int(self.time_interval * 1000) # ms (milli seconds)
        self.Time += self.time_interval # current time in s
        self.Time_ms += self.tt # current time in ms
        # print self.Time_ms

        self.update_paddle_position() 
        self.w.move(self.ball1, dx, dy)
        self.w.move('Monster', dxm, dym)

        # print self.b1.velocity, self.monster1.velocity

        # time loop to continuously update game
        self.w.after(self.tt, self.Update_Ball_Location)

       
root = tk.Tk()
pong_game = Pong(master=root)
pong_game.master.title("Play Pong!")
pong_game.mainloop()
root.destroy() 
    
