
from random import choice
from math import sin, cos, pi, sqrt

random_factor = [.2,.4,.6,.8] # bunch of factors b/w 0 and 1
angle_list = [20,45,90,135,200,225,270,315] # bunch of angles around the circle

class Ball():
    def __init__(self, board_width, board_height, time_interval):
        # create ball with specified radius, color and other initial conditions
        self.radius = 10
        self.color = "red"
        self.initial_conditions(board_width, board_height)
        self.update_position(time_interval)

    def initial_conditions(self, board_width, board_height):
        self.Gx = 0
        self.Gy = 0
        self.G_Angle = 0

        # gives ball velocity with random angle
        # ensures reasonable angle chosen and ball never moves just vertically
        self.velocity = 400
        self.xVelocity = self.velocity*sin(45*pi/180)
        self.yVelocity = self.velocity*cos(45*pi/180)

        self.random_velocity_angle([45,135,225,315])

        # centers ball on the board
        self.xPos = board_width/2
        self.yPos = board_height/2

    def update_position(self, time_interval): 
        # uses the previous velocity to update the position. Affect of gravity also included.
        self.xPos += (self.xVelocity * time_interval + 0.5*self.Gx*time_interval**2)
        self.yPos += (self.yVelocity * time_interval + 0.5*self.Gy*time_interval**2)
        # updating the velocity. If gravity is zero, the velocity is unchanged
        self.xVelocity += self.Gx * time_interval
        self.yVelocity += self.Gy * time_interval

    def random_velocity_angle(self, list1=angle_list): 
        
        Angle = choice(list1)* pi/180
        self.velocity = sqrt(self.xVelocity**2 + self.yVelocity**2)

        self.xVelocity = self.velocity*sin(Angle)
        self.yVelocity = self.velocity*cos(Angle)

    def ball_miss_response(self, board_width, board_height):
        # reinitialize ball w/ centered position and new velocity
        self.initial_conditions(board_width, board_height)

    def increase_speed(self):
        # increases velocity of ball
            self.xVelocity *= 1.5
            self.yVelocity *= 1.5

    # for when the ball hits the top/bottom or side walls
    def bounce_response(self, position, surface1, surface2, board, block):
        if position < board/2: #top/left
            position = self.radius + block #update position
            self.reflect(surface1) #update velocity
        elif position > board/2: #bottom/right
            position = board - self.radius - block
            self.reflect(surface2)

    def reflect(self, surface):
        # uses a tuple for the normal vector of the surface the ball reflects off of
        velocity = (self.xVelocity,self.yVelocity)
        diagonal = -2 * self.dot(surface, velocity)
        if self.dot(surface, velocity) <= 0:
            velocity = self.add(velocity, self.scale(surface, diagonal))
            self.xVelocity = velocity[0]
            self.yVelocity = velocity[1]

    def dot(self, x, y):
        """
        2D dot product
        """
        return x[0]*y[0] + x[1]*y[1]


    def scale(self, x, a):
        """
        2D scalar multiplication
        """
        return (x[0]*a, x[1]*a)


    def add(self, x, y):
        """
        2D vector addition
        """
        return (x[0]+y[0], x[1]+y[1])

    #=================================================

    def Gravity_Response(self, state, time_interval):
        if state == 1: # gravity on
            gg = 500
        else: # gravity off
            gg = 0
        self.G_Angle = choice(angle_list)

        self.Gx = gg*sin(self.G_Angle )
        self.Gy = gg*cos(self.G_Angle )


    def portal_response(self, portal1_position, portal2_position, board_height):
        #changes position of ball to that of the other portal once it goes through
        if self.yVelocity < 0:
            self.yPos = board_height - self.radius - 1
            self.xPos = portal2_position
        elif self.yVelocity > 0:
            self.yPos = self.radius + 1
            self.xPos = portal1_position

#========================================================================
class Monster(Ball):
    def __init__(self, radius, color, board_width, board_height, time_interval):
        Ball.__init__(self, board_width, board_height, time_interval)
        self.radius = radius
        self.color = color
        self.xPos = choice(random_factor)*board_width
        self.yPos = choice(random_factor)*board_height
        self.velocity = 200
        self.xVelocity = self.velocity*sin(135*pi/180)
        self.yVelocity = self.velocity*cos(135*pi/180)
        self.random_velocity_angle()
#========================================================================
    
class Paddle():
    def __init__(self, xPos, color, board_height, up, down):
        # Initialize paddle
        self.length = 100
        self.radius = self.length/2 # for when shape changed to semi-circle
        self.width = 20
        self.color = color
        self.xPos = xPos
        self.initialize_paddle(board_height, up,down)

    def move(self, buttons, board_height):
        # makes up and down buttons for paddles so that pushing and releasing both change velocity
        # this way if you press both buttons you won't move
        in_up_range = (self.yPos - self.length/2 > 0)
        in_down_range = (self.yPos + self.length/2 < board_height)
        if (self.up_key in buttons and self.down_key not in buttons) and in_up_range:
            self.yPos -= self.velocity
        elif (self.down_key in buttons and self.up_key not in buttons) and in_down_range:
            self.yPos += self.velocity

    def center_paddle(self, board_height):
        #position of mathematical paddle defined from the middle of the outside side
        self.yPos = board_height/2

    def initialize_speed(self, up, down):
        self.velocity = 8
        self.up_key = up
        self.down_key = down

    def initialize_paddle(self, board_height, up, down):
        self.velocity = 8
        # reset key control
        self.paddle_key_direction(up, down)
        # centralize paddle
        self.center_paddle(board_height)

    def paddle_key_direction(self, up, down):
        self.up_key, self.down_key = up, down


#========================================================================    
class Portal():
    def __init__(self, yPos, board_width):
        self.width = 200
        self.yPos = yPos
        self.color = "DeepPink2"
        self.random_position(board_width)

    def random_position(self, board_width):
        self.xPos = choice(random_factor)*board_width  
#========================================================================

